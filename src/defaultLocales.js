/*
	text
	email
	numeric: 0-9
	alpha-numeric: a-zA-Z0-9
	tel: 0-9 ()+-*#
*/


export default {
	en: {
		text: [
			[
				['q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p', { action: 'backspace', icon: 'backspace', colSpan: 4 }],
				[{ char: 'a', colSpan: 3, colShift: 1 }, 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '+', { action: 'enter', icon: 'subdirectory_arrow_left', colSpan: 3 }],
				[{ action: 'shift', icon: 'arrow_upward' }, 'z', 'x', 'c', 'v', 'b', 'n', 'm', '.', '_', '-', { action: 'shift', icon: 'arrow_upward' }],
				[{ char: '&123', action: 'meta+' }, { char: ' ', icon: 'space_bar', colSpan: 18 }, { action: 'left', icon: 'keyboard_arrow_left' }, { action: 'right', icon: 'keyboard_arrow_right' }],
			], [
				[{}, { char: '(', colSpan: 2 }, ')', '.', '_', '-', '+', { char: '1', colSpan: 3, colShift: 1 }, '2', '3', { action: 'backspace', icon: 'backspace', colSpan: 3, colShift: 1 }],
				[{ action: 'meta-', icon: 'arrow_left' }, '{', '}', '@', '&', '^', '*', { char: '4', colSpan: 3, colShift: 1 }, '5', '6', { action: 'enter', icon: 'subdirectory_arrow_left', colSpan: 3, colShift: 1, rowSpan: 3 }],
				[{ action: 'meta+', icon: 'arrow_right' }, '[', ']', '!', '#', '?', '=', { char: '7', colSpan: 3, colShift: 1 }, '8', '9'],
				[{ action: 'meta0', char: 'abc' }, { char: ' ', icon: 'space_bar', colSpan: 8 }, { action: 'left', icon: 'keyboard_arrow_left' }, { action: 'right', icon: 'keyboard_arrow_right' }, { char: '0', colSpan: 5, colShift: 1 }, '.'],
			], [
				[{}, { char: '<', colSpan: 2 }, '>', '"', ';', '|', '', { char: '1', colSpan: 3, colShift: 1 }, '2', '3', { action: 'backspace', icon: 'backspace', colSpan: 3, colShift: 1 }],
				[{ action: 'meta-', icon: 'arrow_left' }, '\\', '/', '\'', ':', '~', '', { char: '4', colSpan: 3, colShift: 1 }, '5', '6', { action: 'enter', icon: 'subdirectory_arrow_left', colSpan: 3, colShift: 1, rowSpan: 3 }],
				[{ action: 'meta+', icon: 'arrow_right' }, '%', '£', '$', ',', '`', '', { char: '7', colSpan: 3, colShift: 1 }, '8', '9'],
				[{ action: 'meta0', char: 'abc' }, { char: ' ', icon: 'space_bar', colSpan: 8 }, { action: 'left', icon: 'keyboard_arrow_left' }, { action: 'right', icon: 'keyboard_arrow_right' }, { char: '0', colSpan: 5, colShift: 1 }, '.'],
			]
		]
	}
}