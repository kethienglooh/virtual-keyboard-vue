# virtual-keyboard-vue

> A dependancy free virtual keyboard created specifically for Vue.js

## Table of Contents

- [Installation](#installation)
- [Usage](#usage)
- [Contributing](#contributing)
- [License](#license)


---

# 📦 Installation
## Installation

> Install Dependancies

```shell
yarn add virtual-keyboard-vue
or
npm i -S virtual-keyboard-vue
```

> Register component
```javascript
import 'virtual-keyboard-vue'; //if Vue is globally declared.

---

import VirtualKeyboard from 'virtual-keyboard-vue';

Vue.component('VirtualKeyboard', VirtualKeyboard);
```

# 🚀 Usage
## Usage

> Control a field using ```v-model```
```html
<template>
	<div>
		<input type="date" v-model="field" />
		<VirtualKeyboard layout="text" v-model="field" />
	</div>
</template>

<script>
export default {
	data: () => ({
		field: ""
	})
};
</script>
```

---

> Use the events emitted by the virtual keyboard
```html
<template>
	<VirtualKeyboard layout="text" v-on:char="onChar" v-on:action="onAction" />
</template>

<script>
export default {
	methods: {
		onChar(evt) {
			console.log(evt);
		},
		onAction(evt) {
			console.log(evt);
		}
	}
};
</script>
```

---

> Use the ```v-keyboard``` directive for controlling single or multiple inputs

> Use ```v-keyboard.lock``` to lock the input field down to only the virtual keyboard.
```html
<template>
	<div>
		<input type="text" v-model="text" v-keyboard />
		<input type="email" v-model="email" v-keyboard.lock />
		<VirtualKeyboard /> //Layout will be retrieved from the input field when using this technique.
	</div>
</template>

<script>
export default {
	data: () => ({
		text: "",
		email: ""
	})
};
</script>
```

---

> Specifying a ```locale```
```html
<template>
	//I will only be developing the layouts for the english locale, for now.
	<VirtualKeyboard locale="en" />
</template>
```

---

> Adding a custom ```locale```

> For an example locale configuration, check the ```defaultLocales.js``` file in this repository

```html
<template>
	<div>
		<input type="email" v-keyboard />
		<VirtualKeyboard :locales="locales" locale="es" />
	</div>
</template>

<script>
export default {
	data: () => ({
		locales: {
			es: {
				email: [...]
			}
		}
	})
};
</script>
```

---

> Modifying existing ```locales```

> For an example locale configuration, check the ```defaultLocales.js``` file in this repository

```html
<template>
	<div>
		<input type="email" v-keyboard />
		<VirtualKeyboard :locales="locales" locale="es" />
	</div>
</template>

<script>
const locales = {
	es: {
		email: [...]
	}
};

export default {
	data: () => ({
		locales: (_locales) => {
			//Modify the _locales object
			_locales.en.email[0][3] = 'q';

			//return the final merged locales object
			return Object.assign(_locales, locales);
		}
	})
};
</script>
```

---


# ✅ Contributing
## Contributing

> To get started...

### Step 1

- **Option 1**
    - 🍴 Fork this repo!

- **Option 2**
    - 👯 Clone this repo to your local machine using `https://ranseur92@bitbucket.org/ranseur92/virtual-keyboard-vue.git`

### Step 2

- **HACK AWAY!** 🔨🔨🔨

### Step 3

- 🔃 Create a new pull request.

---


## License

[![License](https://img.shields.io/:license-mit-blue.svg?style=flat-square)](http://badges.mit-license.org)

- **[MIT license](http://opensource.org/licenses/mit-license.php)**